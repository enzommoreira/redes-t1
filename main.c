#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <netinet/in.h> 
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/statvfs.h>

#include "main.h"

#define DEBUG 1
#define NO_DEBUG 0

// ---------- message codes

#define NACK      0
#define ACK       1
#define OK        2
#define DADOS     3
#define DESCRITOR 5
#define PRINT     7
#define GET       10
#define PUT       11
#define CD        12
#define LS        13
#define ERRO      14
#define END       15

// ---------- error codes

#define CD_NOTEXISTS     1
#define CD_PERMISSIONS   2
#define PUT_NOSPACE      3
#define MSG_PARSEERROR   4
#define GET_NOSPACE      5
#define GET_NOTEXISTS    6

#define NO_FLAGS 0

#define MESSAGE_START 0b01111110

#define POLYNOM 0x1D

#define BYTE_SIZE 8
#define SIZE_BITS 7
#define SEQ_BITS 5
#define TYPE_BITS 4

#define MESSAGE_CAP (2 << SEQ_BITS)
#define SIZE_CAP (2 << (SIZE_BITS-1))-1
#define SEQ_CAP (2 << (SEQ_BITS-1))-1

#define CONTROL_BYTES 4

#define MAX_PACKET_SIZE CONTROL_BYTES + SIZE_CAP

#define NO_WAIT 0
#define WAIT 1

#define LS_FRAME_SIZE 1
#define DATA_FRAME_SIZE 3

#define TIMEOUT 2
#define MAX_NACK_ATTEMPTS 20

unsigned char g__seq = 0;
int main_socket;

int timeout = 0;

int is_set_handler = 0;

// ---------- CRC
int check_crc(message_t *message) {
    unsigned char crc = 0;

    for(int i = 0; i < get_packet_size(message); i++)
    {
        crc ^= message->message[i]; /* XOR-in the next input byte */

        for (int i = 0; i < BYTE_SIZE; i++)
        {
            if ((crc & 0x80) != 0)
            {
                crc = (char)((crc << 1) ^ POLYNOM);
            }
            else
            {
                crc <<= 1;
            }
        }
    }

    //printf("[crc check] expected: %d, found: %d\n", message->crc, crc);
    if (message->crc == crc)
        return 1;
    return 0;
}

void create_crc(message_t *message) {
    unsigned char crc = 0;

    for(int i = 0; i < get_packet_size(message); i++)
    {
        crc ^= message->message[i]; /* XOR-in the next input byte */

        for (int i = 0; i < BYTE_SIZE; i++)
        {
            if ((crc & 0x80) != 0)
            {
                crc = (char)((crc << 1) ^ POLYNOM);
            }
            else
            {
                crc <<= 1;
            }
        }
    }

    message->crc = crc;
}

void set_crc(message_t *message) {
    create_crc(message);
    move_crc(message);
}

void move_crc(message_t *message) {
    int message_size = get_message_size(message);
    //printf("-----------------------------\n");
    //printf("[crc creation] message_size: %hhu, size_cap: %d\n", message_size, SIZE_CAP);

    if (message_size < SIZE_CAP) {
        message->data[message_size] = message->crc;
        //printf("[crc creation] moving crc\n");
        //printf("[crc creation] message->data[message_size]: %hhu\n", message->data[message_size]);
    }

    /*for(int i = 0;i < message_size+1;i++)
    {
        printf("%hhu, ", message->data[i]);
    }*/
}

void moveback_crc(message_t *message) {
    int message_size = get_message_size(message);
    //printf("-----------------------------\n");
    //printf("[crc check] message_size: %hhu\n", message_size);

    /*for(int i = 0;i < message_size + 1;i++)
    {
        printf("%hhu, ", message->data[i]);
    }*/

    if (message_size < SIZE_CAP) {
        //printf("[crc check] moving crc back\n");
        //printf("[crc check] message->data[message_size]: %hhu\n", message->data[message_size]);
        message->crc = message->data[message_size];
        message->data[message_size] = 0;
    }
}

// ---------- message
void print_message(message_t *message) {
    //printf("[debug] Message type: %hhu, seq: %hhu, size: %hhu, crc: %hhu\n", message->type, message->sequence, message->size, message->crc);
    //printf("Message data: [%s]\n", message->data);
}

void clear_message(message_t *message) {
    for(int i = message->size + 1; i < MESSAGE_CAP; i++)
    {
        message->message[i] = 0;
    }
}

message_t* create_message(unsigned char type, unsigned char size) {
    message_t* message = malloc(sizeof(message_t));
    
    unsigned char seq = g__seq;
    g__seq = (g__seq + 1) % MESSAGE_CAP;

    message->start = MESSAGE_START;
    message->size = size;
    message->sequence = seq;
    message->type = type;

    return message;
}

int get_message_size(message_t *message) {
    return (message->size); // data_size
}

int get_packet_size(message_t *message) {
    return CONTROL_BYTES + (message->size); //(start + size + seq + type + crc) + data_size
}

message_t* create_ack_message(unsigned char acked_message) {
    message_t* message = create_message(ACK, 1);
    message->data[0] = acked_message;
    set_crc(message);
    return message;
}

message_t* create_nack_message(unsigned char nacked_message) {
    message_t* message = create_message(NACK, 1);
    message->data[0] = nacked_message;
    set_crc(message);
    return message;
}

message_t* create_ok_message(unsigned char oked_message) {
    message_t* message = create_message(OK, 1);
    message->data[0] = oked_message;
    set_crc(message);
    return message;
}

message_t* create_descriptor_message(unsigned char size, char data[]) {
    message_t* message = create_message(DESCRITOR, size);
    
    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);
    return message;
}

message_t* create_print_message(unsigned char size, char data[]) {
    message_t* message = create_message(PRINT, size);
    
    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);

    return message;
}

message_t* create_data_message(unsigned char size, char data[]) {
    message_t* message = create_message(DADOS, size);
    
    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);

    return message;
}

message_t* create_get_message(unsigned char size, char data[]) {
    message_t* message = create_message(GET, size);
    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);  

    return message;
}

message_t* create_put_message(unsigned char size, char data[]) {
    message_t* message = create_message(PUT, size);
    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);  

    return message;
}

message_t* create_cd_message(unsigned char size, char data[]) {
    message_t* message = create_message(CD, size);

    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);  

    return message;
}

message_t* create_ls_message(unsigned char size, char data[]) {
    message_t* message = create_message(LS, size);

    for (int i = 0; i < size; i++) {
        message->data[i] = data[i];
    }

    set_crc(message);

    return message;
}

message_t* create_error_message(unsigned char error_code) {
    message_t* message = create_message(ERRO, 1);
    message->data[0] = error_code;
    set_crc(message);
    return message;
}

message_t* create_end_message() {
    message_t* message = create_message(END, 0);
    set_crc(message);
    return message;
}

message_t* create_null_message() {
    message_t* message = malloc(sizeof(message_t));
    return message;
}

int create_message_list(unsigned short type, FILE *input, message_t ***message_list) {
    int i = 0;
    int size = get_file_size(input);

    //printf("Message list! File size: %d\n", size);

    int message_data_size = 0;
    int message_count = (size + SIZE_CAP - 1) / (SIZE_CAP-1) + 1;

    //printf("Message list! Message count: %d\n", message_count);

    char buffer[SIZE_CAP];

    *message_list = malloc((message_count+1) * sizeof(message_t *));

    if (type == PRINT) {
        for (i = 0; i < message_count; i++) {
            printf("Creating message [%d]\n", i);
            message_data_size = read_file_characters(input, buffer, SIZE_CAP);
            (*message_list)[i] = create_print_message(message_data_size, buffer);
        }
    } else if (type == DADOS) {
        for (i = 0; i < message_count; i++) {
            printf("Creating message [%d]\n", i);
            message_data_size = read_file_characters(input, buffer, SIZE_CAP);
            (*message_list)[i] = create_data_message(message_data_size, buffer);
        }
    }

   (*message_list)[i] = create_end_message();

    return message_count + 1;
}

// ---------- file manipulation

unsigned char read_file_characters(FILE *file, char buffer[], int buffer_size) {
    int i = 0;
    char c;
    
    fread(&c, sizeof(char), 1, file);

    while (!feof(file)) {
        //printf("Reading file: current char = [%c]\n", c);
        buffer[i] = c;
        i++;
        if (i == (buffer_size-1))
            break;
        fread(&c, sizeof(char), 1, file);
    }

    buffer[i] = '\0';

    //printf("Reading file: current buffer = [%s]\n\n", buffer);

    return i;
}

FILE *open_file(char file_name[], char permissions[], int *success) {
    FILE *fp;
    printf ("Opening file with name %s\n", file_name);
    fp = fopen (file_name, permissions);
    if (fp == NULL) {
        printf ("Something is wrong with our file!\n");
        message_t *message = create_error_message(GET_NOTEXISTS);
        send_single_message(message, NO_WAIT);
        *success = 0;
        return NULL;
    }
    *success = 1;
    return fp;
}

int get_file_size(FILE *file) {
    fseek(file, 0, SEEK_END);
    int size = ftell(file);
    fseek(file, 0, SEEK_SET);
    return size;
}

// ---------- comms

message_t *send_single_message(message_t *message, int should_wait) {
    int success = 0;

    message_t *response;

    set_timeout_handler(force_timeout);

    if (should_wait) {
        response = malloc(sizeof(message_t));

        //printf("Sending message: ");
        //print_message(message);
        send(main_socket, message->message, (get_packet_size(message) < 64) ? 64 : get_packet_size(message)+1, 0); 
    
        while(!success)
        {
            if (timeout) {
                //printf("Sending message: ");
                //print_message(message);
                send(main_socket, message->message, (get_packet_size(message) < 64) ? 64 : get_packet_size(message)+1, 0); 
            }
            
            set_timeout(TIMEOUT);
            while(recv(main_socket, response->message, sizeof(response->message)+1, NO_FLAGS) < 0 && !timeout){};
			alarm(0);

            if (response->start == MESSAGE_START) {
                moveback_crc(response);
                //print_message(response);
                if (check_crc(response))
                    success = 1;
            }

        }
    } else {
        //printf("Sending message: ");
        //print_message(message);
        send(main_socket, message->message, (get_packet_size(message) < 64) ? 64 : get_packet_size(message)+1, 0); 
    }

    return response;
}

void send_multiple_messages(message_t **message_list, int messages_count, int frame_size) {
    int i = 0;
    int j = 0;
    int success = 0;

    int real_frame_size = 0;

    message_t *frame[frame_size];
    message_t *response = malloc(sizeof(message_t));

    set_timeout_handler(force_timeout);

    int num_frames = ((messages_count + frame_size - 1) / (frame_size));

    for(i = 0; i < num_frames; i++) {
        //printf("\nSending frame %d from %d frames\n", i, num_frames);
        success = 0;
        // preenche o frame
        real_frame_size = ((i+1)*frame_size < messages_count) ? frame_size : frame_size - ((i+1)*frame_size - messages_count);
        
        for(j = 0; j < real_frame_size; j++)
        {
            frame[j] = message_list[i*frame_size + j];
        }

        for(j = 0; j < real_frame_size; j++)
        {
            //printf("\nSending frame %d message %d with size %d: ", i, j, get_packet_size(frame[j]));
           // print_message(frame[j]);
            // envia mensagens
            printf("Sending message %d/%d\n", i*frame_size+j, messages_count);
            send(main_socket, frame[j]->message, (get_packet_size(frame[j]) < 64) ? 64 : get_packet_size(frame[j])+1, 0); 
        }

        while(!success) {
            // tenta enviar frame
            if (timeout) {
                for(j = 0; j < real_frame_size; j++)
                {
                    //printf("\nSending frame %d message %d with size %d: ", i, j, get_packet_size(frame[j]));
                    //print_message(frame[j]);
                    // envia mensagens
                    printf("Sending message %d/%d\n", i*frame_size+j, messages_count);
                    send(main_socket, frame[j]->message, (get_packet_size(frame[j]) < 64) ? 64 : get_packet_size(frame[j])+1, 0); 
                }
            }

            set_timeout(TIMEOUT);
            while(recv(main_socket, response->message, sizeof(response->message)+1, NO_FLAGS) < 0 && !timeout);
            alarm(0);

            if (response->start == MESSAGE_START) {
                moveback_crc(response);

                //printf("Received message: ");
                //print_message(response);
                if (response->type == NACK) {
                    for(j = 0; j < real_frame_size; j++)
                    {
                        if (frame[j]->sequence >= response->data[0] || frame[j]->sequence < response->data[0] - frame_size) {
                            //printf("\nResending frame %d message %d: ", i, j);
                            //print_message(frame[j]);
                            send(main_socket, frame[j]->message, (get_packet_size(frame[j]) < 64) ? 64 : get_packet_size(frame[j])+1, 0); 
                        }
                    }
                } else if (response->type == ACK) {
                    //printf("[ACK] response->data[0]: %d, frame[real_frame_size-1]->sequence: %d", response->data[0], frame[real_frame_size-1]->sequence);
                    if (response->data[0] == frame[real_frame_size-1]->sequence)
                        success = 1; 
                    else {
                        for(j = 0; j < real_frame_size; j++)
                        {
                            if (frame[j]->sequence >= response->data[0] || frame[j]->sequence < response->data[0] - frame_size) {
                                //printf("\nResending frame %d message %d: ", i, j);
                                //print_message(frame[j]);
                                send(main_socket, frame[j]->message, (get_packet_size(frame[j]) < 64) ? 64 : get_packet_size(frame[j])+1, 0); 
                            }
                        }
                    }
                }
            }
        }
    }
}



void wait_single_message(message_t *message) {
    int success = 0;

    set_timeout_handler(force_timeout);

    while(!success)
    { 
        set_timeout(0.01);
        while(recv(main_socket, message->message, sizeof(message->message)+1, NO_FLAGS) < 0 && !timeout);
        alarm(0);

        if (message->start == MESSAGE_START) {

            moveback_crc(message);
            //printf("Received message: ");
            //print_message(message);
            if (check_crc(message)) {
                success = 1;
            } else {
                send_nack_message(message->sequence);
            }
        }
    }
}

int wait_n_print_multiple_messages(FILE *output, int frame_size, int debug) {
    int i = 0;
    int waiting = 1;
    int last_correct_message = 0;
    int read_messages = 0;
    int has_nacked = 0;
    int messages = 0;
    int nack_attempts = 0;

    int last_printed_message = -1;

    message_t *frame[frame_size];
    for(i = 0;i < frame_size;i++)
    {
        frame[i] = malloc(sizeof(message_t));
    }

    while (waiting && nack_attempts < MAX_NACK_ATTEMPTS) {

        has_nacked = 0;

        for(i = 0;i < frame_size;i++)
        {
            while(recv(main_socket, frame[i]->message, sizeof(frame[i]->message)+1, NO_FLAGS) < 0);
            if (frame[i]->start == MESSAGE_START && frame[i]->type == END) {
                i++;
                break;
            }
        }

        read_messages = i; //past: messages that have been received

        for(i = 0;i < read_messages;i++)
        {
            if (frame[i]->start == MESSAGE_START) {
                moveback_crc(frame[i]);
                //printf("Received message: ");
                //print_message(frame[i]);
                if (check_crc(frame[i])) { //msg ok
                    if (debug)
                        printf("Processing message %d\n", messages);
                    last_correct_message = frame[i]->sequence;
                    messages++;
                    if (frame[i]->type == END)
                        waiting = 0;
                    else {
                        if (should_print(last_printed_message, frame[i]->sequence, frame_size)) {
                            //printf("Tá printando frame[%d]\n", i);
                            //printf("Dados: [%s]\n", frame[i]->data);
                            fwrite(frame[i]->data, sizeof(char), frame[i]->size, output);
                            last_printed_message = frame[i]->sequence;
                        }
                    }
                } else {
                    //printf("CRC verification error!\n");
                    has_nacked = 1;
                    nack_attempts++;
                    send_nack_message(frame[i]->sequence);
                    break;
                }
            }
        }

        // sends ack
        if (frame[0]->start == MESSAGE_START && !has_nacked) {
            //printf("Well, i'm gonna send an ack just in case for message (%d). Last printed message is (%d) tho. Actually sendin to (%d)\n", last_correct_message, last_printed_message, (last_correct_message > last_printed_message || (last_correct_message >= SIZE_CAP && last_printed_message < SIZE_CAP - frame_size)) ? last_correct_message : last_printed_message);
            //print_message(frame[0]);
            nack_attempts = 0;
            send_ack_message((last_correct_message > last_printed_message || (last_correct_message >= SIZE_CAP && last_printed_message < SIZE_CAP - frame_size)) ? last_correct_message : last_printed_message);
        }

        //resets frame
        for(i = 0;i < frame_size;i++)
        {
            memset(frame[i], 0, sizeof(message_t));
        }
    }

    if (nack_attempts >= MAX_NACK_ATTEMPTS)
        return 0;
    return 1;
}

int should_print(int last_printed, int current, int frame_size) {
    //printf("\nShould print? %d, %d, %d, MAX: %d\n", last_printed, current, frame_size, SEQ_CAP);
    if (last_printed == -1) {
        //printf("Ok, printing this one!\n");
        return 1;
    }

    if (last_printed >= (SEQ_CAP) - frame_size) {
        //printf("Border case warning!\n");
        if (current < (last_printed - frame_size) || current > last_printed) {
            //printf("Ok, printing this one! It's a border case\n");
            return 1;
        }
    } else {
        if (current > last_printed) {
            //printf("Ok, printing this one! It's just a normal case\n");
            return 1;
        }
    }
    //printf("Ok, not printing this one!\n");
    return 0;
}

// ---------- high level message functions

void send_nack_message(unsigned char seq) {
    message_t* message = create_nack_message(seq);
    send_single_message(message, NO_WAIT);
    free(message);
}

void send_ack_message(unsigned char seq) {
    message_t* message = create_ack_message(seq);
    send_single_message(message, NO_WAIT);
    free(message);
}

void send_ok_message(unsigned char seq) {
    message_t* message = create_ok_message(seq);
    send_single_message(message, NO_WAIT);
    free(message);
}

void send_cd_message(char dir_name[]) {
    int size = strlen(dir_name) + 1;
    message_t* message = create_cd_message(size, dir_name);
    message_t* response = send_single_message(message, WAIT);
    if (response->type == ERRO) {
        printf("\nError executing remote cd!\n");
    } else if (response->type == OK) {
        printf("\nSucessfully executed remote cd!\n");
    }
    free(message);
    free(response);
}

void send_put_message(char file_name[]) {
    int size = strlen(file_name) + 1;
    int success = 0;
    message_t* message = create_put_message(size, file_name);
    message_t* response = send_single_message(message, WAIT);

    if (response->type == ERRO) {
        printf("\nError executing PUT!\n");
    } else if (response->type == OK) {

        FILE *file = open_file(file_name, "rb", &success);
        if (success) {
            int file_size = get_file_size(file);
            char buffer[128]; 
            sprintf(buffer, "%d%s", file_size, "\0");

            message = create_descriptor_message(strlen(buffer), buffer);
            message_t* response = send_single_message(message, WAIT);

            if (response->type == ERRO) {
                printf("\nError sending descriptor!\n");
            } else {
                message_t **message_list = NULL;
                int messages_count = create_message_list(DADOS, file, &message_list);
                printf("Sending %d messages!\n", messages_count);
                send_multiple_messages(message_list, messages_count, DATA_FRAME_SIZE);
                printf("\nSuccesfully sent file!\n");
            }
        }
    }

    free(message);
    free(response);
}

void send_get_message(char file_name[]) {
    int size = strlen(file_name) + 1;
    int success = 0;
    message_t* message = create_get_message(size, file_name);
    message_t* response = send_single_message(message, WAIT);

    if (response->type == ERRO) {
        printf("\nError getting descriptor!\n");
    } else if (response->type == DESCRITOR) {
        unsigned long file_size = -1;
        str_to_ulong(&file_size, (char *)response->data, 10);

        unsigned long free_space = -1;
        free_space = get_free_space(".");

        if (file_size < 0) {
            printf("\nError getting file size!\n");
            printf("\nFile size: %ld, free space: %ld\n", file_size, free_space);
            printf("\nFile size as message says: %s\n", response->data);
            send_single_message(message, NO_WAIT);
        } else if (free_space < 0) {
            printf("\nError getting free disk space!\n");
            message = create_error_message(PUT_NOSPACE);
            send_single_message(message, NO_WAIT);
        } else if (file_size > free_space) {
            printf("\nError getting file size!\n");
            printf("\nFile size: %ld, free space: %ld\n", file_size, free_space);
            printf("\nFile size as message says: %s\n", response->data);
            message = create_error_message(PUT_NOSPACE);
            send_single_message(message, NO_WAIT);
        } else {
            FILE *file = open_file(file_name, "wb+", &success);

            message = create_ok_message(response->sequence);
            send_single_message(message, NO_WAIT);

            if (wait_n_print_multiple_messages(file, DATA_FRAME_SIZE, DEBUG))
                printf("\nSuccesfully got file!\n");
            else {
                printf("Problem getting file: prohibited bit sequence\n");
                exit(3);
            }
            fclose(file);
        }
    }

    free(message);
    free(response);
}


void send_ls_message(char command[]) {
    int size = strlen(command) + 1;
    message_t* message = create_ls_message(size, command);
    send_single_message(message, NO_WAIT);
    printf("\n");
    if (!wait_n_print_multiple_messages(stdout, DATA_FRAME_SIZE, NO_DEBUG)) {
        printf("Problem getting LS: prohibited bit sequence\n");
        exit(3);
    }
    printf("\n");
}

void send_master_ls_messages(FILE *output) {
    message_t **message_list = NULL;
    int messages_count = create_message_list(PRINT, output, &message_list);
    printf("Sending %d messages!\n", messages_count);
    send_multiple_messages(message_list, messages_count, DATA_FRAME_SIZE);
    printf("\nSuccesfully sent LS_FRAME_SIZE!\n");
}

void send_error_message(unsigned char seq, unsigned char error_code) {
    message_t* message = create_error_message(error_code);
    send_single_message(message, NO_WAIT);
}


// ----------

int change_directory(char buffer[256]) {
   return chdir(buffer);
}

// ---------- slave



void slave_loop() {
    message_t *message = malloc(sizeof(message_t));

    while(1) {
        printf("Waiting message...\n");
        wait_single_message(message);
        
        if (message->type == CD) {
            printf("Receiving remote CD\n");
            slave_remote_cd(message);
        }
        else if (message->type == LS) {
            printf("Receiving remote LS\n");
            slave_remote_ls(message);
        }
        else if (message->type == GET) {
            printf("Receiving GET\n");
            slave_get(message);
        }
        else if (message->type == PUT) {
            printf("Receiving PUT\n");
            slave_put(message);
        }
    }
}


// ---------------

void slave_remote_cd(message_t *message) {
    int error = change_directory((char *)message->data);
    if (error) {
        switch (errno) {
            case EACCES: 
                send_error_message(message->sequence, CD_PERMISSIONS);
            break;
            case ENOENT:
                send_error_message(message->sequence, CD_NOTEXISTS);
            break;
        }
    } else {
        printf("Directory changed\n");
        send_ok_message(message->sequence);
    }
}

void slave_remote_ls(message_t *message) {
    char buffer[512];
    sprintf(buffer, "%s%s", message->data, "> .ls.tmp");
    system(buffer);
    int success = 0;
    FILE* output = open_file(".ls.tmp", "r", &success);
    send_master_ls_messages(output);
    printf("Successfully sending LS\n");
    system("rm .ls.tmp");
}

void slave_get(message_t *message) {
    int success = 0;
    FILE *file = open_file((char *) message->data, "rb", &success);

    if (success) {
        int file_size = get_file_size(file);
        char buffer[128]; 
        sprintf(buffer, "%d%s", file_size, "\0");

        message = create_descriptor_message(file_size, buffer);
        message_t* response = send_single_message(message, WAIT);

        if (response->type == ERRO) {
            printf("\nError sending descriptor!\n");
        } else {
            message_t **message_list = NULL;
            int messages_count = create_message_list(DADOS, file, &message_list);
            send_multiple_messages(message_list, messages_count, DATA_FRAME_SIZE);

            printf("Succesfully sent file\n");
        }
        fclose(file);
    }

}

void slave_put(message_t *message) {
    int success = 0;

    char *file_name_buffer = malloc(sizeof(char) * SIZE_CAP);
    memcpy(file_name_buffer, message->data, SIZE_CAP); 

    message = create_ok_message(message->sequence);
    message_t* response = send_single_message(message, WAIT);

    if (response->type == ERRO) {
        printf("\nError getting descriptor!\n");
    } else if (response->type == DESCRITOR) {
        
        unsigned long file_size = -2;
        str_to_ulong(&file_size, (char *)response->data, 10);

        unsigned long free_space = -1;
        free_space = get_free_space(".");

        if (file_size < 0) {
            printf("\nError getting file size!\n");
            printf("\nFile size: %ld, free space: %ld\n", file_size, free_space);
            printf("\nFile size as message says: %s\n", response->data);
            message = create_error_message(MSG_PARSEERROR);
            send_single_message(message, NO_WAIT);
        } else if (free_space < 0) {
            printf("\nError getting free disk space!\n");
            message = create_error_message(PUT_NOSPACE);
            send_single_message(message, NO_WAIT);
        } else if (file_size > free_space) {
            printf("\nError getting file size!\n");
            printf("\nFile size: %ld, free space: %ld\n", file_size, free_space);
            printf("\nFile size as message says: %s\n", response->data);
            message = create_error_message(PUT_NOSPACE);
            send_single_message(message, NO_WAIT);
        } else {

            FILE *file = open_file(file_name_buffer, "wb+", &success);
            message = create_ok_message(response->sequence);
            send_single_message(message, NO_WAIT);
            
            if (wait_n_print_multiple_messages(file, DATA_FRAME_SIZE, DEBUG))
                printf("Succesfully received file\n");
            else {
                printf("Problem getting file: prohibited bit sequence\n");
                exit(3);
            }

            fclose(file);
        }

    }

    free(file_name_buffer);
}

// --------------

unsigned long str_to_ulong(unsigned long *out, char *s, int base) {
    char *end;
    if (s[0] == '\0' || s[0] == ' ')
        return -1;
    errno = 0;
    unsigned long l = strtol(s, &end, base);
    if (*end != '\0')
        return -1;
    *out = l;
    return 1;
}

unsigned long get_free_space(char dir[]) {
    struct statvfs buffer;

    if (!statvfs(dir, &buffer)) {
        return buffer.f_bsize * buffer.f_bfree;
    }

    return -1;
}
// --------------

void raw_socket_connection(char *device)
{
    int soquete;
    struct ifreq ir;
    struct sockaddr_ll endereco;
    struct packet_mreq mr;

    soquete = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));  	/*cria socket*/
    if (soquete == -1) {
        printf("Erro no Socket\n");
        exit(-1);
    }

    memset(&ir, 0, sizeof(struct ifreq));  	/*dispositivo eth0*/
    memcpy(ir.ifr_name, device, (size_t) sizeof(device));
    if (ioctl(soquete, SIOCGIFINDEX, &ir) == -1) {
        printf("Erro no ioctl\n");
        exit(-1);
    }
    memset(&endereco, 0, sizeof(endereco)); 	/*IP do dispositivo*/
    endereco.sll_family = AF_PACKET;
    endereco.sll_protocol = htons(ETH_P_ALL);
    endereco.sll_ifindex = ir.ifr_ifindex;
    if (bind(soquete, (struct sockaddr *)&endereco, sizeof(endereco)) == -1) {
        printf("Erro no bind\n");
        exit(-1);
    }
    memset(&mr, 0, sizeof(mr));          /*Modo Promiscuo*/
    mr.mr_ifindex = ir.ifr_ifindex;
    mr.mr_type = PACKET_MR_PROMISC;
    if (setsockopt(soquete, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)	{
        printf("Erro ao fazer setsockopt\n");
        exit(-1);
    }

    main_socket = soquete;
}

void force_timeout() {
	timeout = 1;
}

void set_timeout_handler(void (*func)()) {
	if (is_set_handler)
		return;

	struct sigaction action;

	action.sa_handler = func;
	sigemptyset (&action.sa_mask);
	action.sa_flags = 0;

	if(sigaction(SIGALRM, &action, 0) < 0)
	{
		exit(1);
	}

	is_set_handler = 1;
}

void set_timeout(unsigned int seconds) {
	alarm(0);
	alarm(seconds);

	timeout = 0;
}