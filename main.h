#ifndef __MAIN_H__
#define __MAIN_H__

// ------- struct da mensagem
typedef struct message_t {
	union {
		unsigned char message[131];
		struct {
			unsigned char start;
			union {
				unsigned char header[2];
				struct {
					unsigned char size:7;
					unsigned char sequence:5;
					unsigned char type:4;
				};
			};
			unsigned char data[127]; 
            unsigned char crc;
		};
	};
} message_t;

FILE *open_file(char file_name[], char permissions[], int *success);
message_t *send_single_message(message_t *message, int should_wait);
int change_directory(char buffer[256]);
int check_crc(message_t *message);
void clear_message(message_t *message);
message_t* create_ack_message(unsigned char acked_message);
message_t* create_cd_message(unsigned char size, char data[]);
void create_crc(message_t *message);
message_t* create_descriptor_message(unsigned char size, char data[]);
message_t* create_end_message();
message_t* create_error_message(unsigned char error_code);
message_t* create_get_message(unsigned char size, char data[]);
message_t* create_ls_message(unsigned char size, char data[]);
int create_message_list(unsigned short type, FILE *output, message_t ***message_list);
message_t* create_message(unsigned char type, unsigned char size);
message_t* create_nack_message(unsigned char nacked_message);
message_t* create_null_message();
message_t* create_ok_message(unsigned char oked_message);
message_t* create_print_message(unsigned char size, char data[]);
message_t* create_put_message(unsigned char size, char data[]);
int get_file_size(FILE *file);
int get_message_size(message_t *message);
int get_packet_size(message_t *message);
void move_crc(message_t *message);
void moveback_crc(message_t *message);
void print_message(message_t *message);
void raw_socket_connection(char *device);
unsigned char read_file_characters(FILE *file, char buffer[], int buffer_size);
void send_ack_message(unsigned char seq);
void send_cd_message(char dir_name[]);
void send_error_message(unsigned char seq, unsigned char error_code);
void send_get_message(char file_name[]);
void send_ls_message(char command[]);
void send_master_ls_messages(FILE *output);
void send_multiple_messages(message_t **message_list, int messages_count, int frame_size);
void send_nack_message(unsigned char seq);
void send_ok_message(unsigned char seq);
void send_put_message(char file_name[]);
void set_crc(message_t *message);
void set_socket_timeout();
void slave_get(message_t *message);
void slave_loop();
void slave_put(message_t *message);
void slave_remote_cd(message_t *message);
void slave_remote_ls(message_t *message);
int wait_n_print_multiple_messages(FILE *output, int frame_size, int debug);
void wait_single_message(message_t *message);
message_t* create_data_message(unsigned char size, char data[]);
int should_print(int last_printed, int current, int frame_size);
unsigned long str_to_ulong(unsigned long  *out, char *s, int base);
unsigned long get_free_space(char dir[]);
void set_timeout_handler(void (*handler_func)());
void set_timeout(unsigned int seconds);
void force_timeout();

#endif