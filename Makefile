CFLAGS = -Wall -g

TARGETS = master slave

objs = main.o master.o slave.o
files = main.c master.c slave.c main.h

all: $(TARGETS)

master: $(objs)
	$(CC) main.o master.o -o master $(LDFLAGS)

slave: $(objs)
	$(CC) main.o slave.o -o slave $(LDFLAGS)

main.o: main.c main.h
	gcc $(CFLAGS) main.c -c

master.o: master.c main.h
	gcc $(CFLAGS) master.c -c

slave.o: slave.c main.h
	gcc $(CFLAGS) slave.c -c

clean:
	$(RM) -v master slave $(objs) -r
