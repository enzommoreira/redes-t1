#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "main.h"

#define DEVICE "eno1"

void get_input_name(char input_type[], char buffer[]) {
    printf("%s name: ", input_type);
    scanf("%s", buffer);
}

void local_cd_menu() {
    char buffer[256];
    get_input_name("Directory", buffer);
    change_directory(buffer);
}

void local_ls_menu() {
    char flag_buffer[12];
    char dest[32];

    printf("Flags: -a, -l, -la, (none = .)\n");
    printf("> ");
    scanf("%10s", flag_buffer);

    sprintf(dest, "%s%s", "ls ", flag_buffer);

    printf("\n");
	system(dest);
    printf("\n");
}

void remote_cd_menu() {
    char buffer[128];
    get_input_name("Directory", buffer);
    send_cd_message(buffer);
}

void remote_ls_menu() {
    char flag_buffer[12];
    char dest[32];

    printf("Flags: -a, -l, -la, (none = .)\n");
    printf("> ");
    scanf("%10s", flag_buffer);

    sprintf(dest, "%s%s", "ls ", flag_buffer);

    send_ls_message(dest);
}

void get_file_menu() {
    char buffer[128];
    get_input_name("File", buffer);
    send_get_message(buffer);
}

void put_file_menu() {
    char buffer[128];
    get_input_name("File", buffer);
    send_put_message(buffer);
}

int main() {
    printf("Starting master...\n");

    // creates connection 
    raw_socket_connection(DEVICE);

    int running = 1;
    char command_buffer[32];

    while(running){
        printf("Commands: cd, ls, rcd, rls, get, put, exit\n");
        printf("> ");
        scanf("%10s", command_buffer);
        if (strcmp(command_buffer,"cd") == 0) {
            printf("\n> Local cd\n");
            local_cd_menu();
        }
        else if (strcmp(command_buffer,"ls") == 0) {
            printf("\n> Local ls\n");
            local_ls_menu();
        }
        else if (strcmp(command_buffer,"rcd") == 0) {
            printf("\n> Remote cd\n");
            remote_cd_menu();
        }
        else if (strcmp(command_buffer,"rls") == 0) {
            printf("\n> Remote ls\n");
            remote_ls_menu();
        }
        else if (strcmp(command_buffer,"get") == 0) {
            printf("\n> Get file\n");
            get_file_menu();
        }
        else if (strcmp(command_buffer,"put") == 0) {
            printf("\n> Put file\n");
            put_file_menu();;
        }
        else if (strcmp(command_buffer,"exit") == 0) {
            printf("\nCya!\n");
            running = 0;
        }
        else{ 
            printf("\nUnknown command!\n");
        }
    }

    return 0;
}
