#include <stdio.h>

#include "main.h"

#define DEVICE "eno1"

int main() {
    printf("Starting slave...\n");

    raw_socket_connection(DEVICE);
    
    slave_loop();

    return 0;
}